package com.example.rezaesmaeeli.sematec.Session3;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rezaesmaeeli.sematec.R;

public class TestPutExtraActivity extends AppCompatActivity {

    EditText myTXT;
    Button myBTN;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_put_extra);
        myTXT = findViewById(R.id.go_back_text);
        myBTN = findViewById(R.id.go_back);

        myBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent bb = new Intent();
                String TempNew =myTXT.getText().toString();
                bb.putExtra("myExtra",TempNew);
                setResult(Activity.RESULT_OK,bb);
                finish();
            }
        });

        /*Intent i =getIntent();
        String temp = i.getStringExtra("val");
        Toast.makeText(this, ""+temp, Toast.LENGTH_SHORT).show();*/
    }
}
