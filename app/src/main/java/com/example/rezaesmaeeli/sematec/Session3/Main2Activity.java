package com.example.rezaesmaeeli.sematec.Session3;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.rezaesmaeeli.sematec.R;
import com.orhanobut.hawk.Hawk;
import com.squareup.picasso.Picasso;

public class Main2Activity extends AppCompatActivity {

    EditText myEdit;
    Button btn;
    ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Hawk.init(this).build();
        Hawk.put("key", "value");
       // Hawk.get("Key", "def");
        myEdit = findViewById(R.id.my_edit_text);
        btn = findViewById(R.id.my_btm_redirect);
        img=findViewById(R.id.my_img);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempText = myEdit.getText().toString();
                //Intent myIntent = new Intent(Main2Activity.this,TestPutExtraActivity.class);
                //myIntent.putExtra("val",tempText);

                // startActivityForResult(myIntent,150);
                // startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),150);
                //Intent goToURL=new Intent(Intent.ACTION_VIEW, Uri.parse(tempText));
                //startActivity(goToURL);

                /*PreferenceManager.getDefaultSharedPreferences(Main2Activity.this)
                        .edit().putString("name",tempText).apply();
*/
               /* String u=PreferenceManager.getDefaultSharedPreferences(Main2Activity.this)
                        .getString("name","defaultValue[Reza]");
                Toast.makeText(Main2Activity.this, ""+u, Toast.LENGTH_SHORT).show();*/

                Picasso.get().load("http://labsafety.rezaesmaeeli.ir/Areas/User/Content/LoginAssistant/img/Customer-Service-Girl-3-e1478208374989.png")
                .into(img);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 150) {
            Toast.makeText(this, "yes", Toast.LENGTH_SHORT).show();
            if (resultCode == Activity.RESULT_OK) {
                //String tt = data.getStringExtra("myExtra");
                /*Toast.makeText(this, ""+tt, Toast.LENGTH_SHORT).show();*/

            }
        }
    }
}
